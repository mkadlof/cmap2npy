import os
import h5py
import numpy as np


def cmap2npy(args):
    for i, fname in enumerate(args.input_files):
        f = h5py.File(fname)
        img = f['Chimera/image1/data_zyx'].value
        if args.verbose and 'Chimera/image1/Statistics' in f:
            attributes = dict(f['Chimera/image1/Statistics'].attrs)
            for atr in attributes:
                print('{:<15}: {}'.format(atr, attributes[atr]))
        print(fname)

        inv = ''
        if args.invert:
            img = -img
            inv = '_inverted'

        if args.output_file_name and len(args.input_files) == 1:
            outfile = '{}{}.npy'.format(args.output_file_name, inv)
        elif args.output_file_name and len(args.input_files) > 1:
            outfile = '{}_{}{}.npy'.format(args.output_file_name, i, inv)
        elif not args.output_file_name and len(args.input_files) == 1:
            base, _ = os.path.splitext(fname)
            outfile = '{}{}.npy'.format(base, inv)
        else:
            base, _ = os.path.splitext(fname)
            outfile = '{}_{}{}.npy'.format(base, i, inv)

        np.save(outfile, img)
        print(img.shape)
        print('File {} saved.'.format(outfile))
