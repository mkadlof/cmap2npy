#! /usr/bin/env python3

import argparse
from cmap2npy import cmap2npy


def main():
    parser = argparse.ArgumentParser(description="Convert cmap file to numpy npy file.")
    parser.add_argument("-o", '--output_file_name', help="Output npy file name without '.npy' extension!")
    parser.add_argument("-i", '--invert', action="store_true", default=False, help="Map inverting")
    parser.add_argument("-v", '--verbose', action="store_true", default=False, help="verbose output")
    parser.add_argument("input_files", metavar="input_file", nargs='+', help="cmap files")
    args = parser.parse_args()

    cmap2npy(args)


if __name__ == '__main__':
    main()
