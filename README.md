# README #

This code converts UCSF volumetric data `*.cmap` into numpy 3D array file format `*.npy`.

### Requirements and setup ###

This script require python 3.x

Setup:
`pip install -r requirements.txt`

### Misc ###
Licence: Do with it whatever you want.
This code is availiable at https://bitbucket.org/mkadlof/cmap2npy
